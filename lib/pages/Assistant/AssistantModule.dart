import 'package:flutter/material.dart';
import 'package:speech_recognition/speech_recognition.dart';
import 'package:flutter_dialogflow/dialogflow_v2.dart';
import 'package:flutter_tts/flutter_tts.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: VoiceHome(),
    );
  }
}

class VoiceHome extends StatefulWidget {
  @override
  _VoiceHomeState createState() => _VoiceHomeState();
}

class _VoiceHomeState extends State<VoiceHome> {
  SpeechRecognition _speechRecognition;
  bool _isAvailable = false;
  bool _isListening = false;
  bool statusSpeck = true;

  String resultText = "";

  @override
  void initState() {
    super.initState();
    initSpeechRecognizer();
  }

  void initSpeechRecognizer() {
    _speechRecognition = SpeechRecognition();

    _speechRecognition.setAvailabilityHandler(
      (bool result) => setState(() => _isAvailable = result),
    );

    _speechRecognition.setRecognitionStartedHandler(
      () => {
        setState(() => _isListening = true),
        }
    );

    _speechRecognition.setRecognitionResultHandler((String speech) => {
          setState(() => resultText = speech),
          Response(resultText),
        });


    _speechRecognition.setRecognitionCompleteHandler(
      () => setState(() => _isListening = false),
    );

    _speechRecognition.activate().then(
          (result) => setState(() => _isAvailable = result),
        );
  }

  final FlutterTts flutterTts = FlutterTts();
  void Speak(text) async {
    await flutterTts.setLanguage("th-TH");
    // await flutterTts.setPitch(-1.00);
    await flutterTts.speak(text);
  }

  void GetText() async {
    await _speechRecognition.listen(locale: "th_TH").then((result) => {
          print("1111111111111111111111111111111 : " + resultText),
          print('$result')
        });
    print("22222222222222222222222 : " + resultText);
  }

  void Response(query) async {
    print("STATUS +>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
    print(statusSpeck);
    if (statusSpeck) {
      try {
        
       statusSpeck = false;
        AuthGoogle authGoogle =
            await AuthGoogle(fileJson: "asset/dialogflow-api.json").build();
        Dialogflow dialogflow =
            Dialogflow(authGoogle: authGoogle, language: Language.thai);
        AIResponse response = await dialogflow.detectIntent(query);
        print("2");
        Speak(response.getMessage());
        statusSpeck = true;
      } catch (e) {
        statusSpeck = true;
        // // null msg case
        // print("NULL MESSAGE");
        // Speak("ฉันฟังไม่ชัดหรือท่านยังไม่ได้ถามอะไร");
      }
      
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Text(
              "คุณได้พูดว่า",
              style: TextStyle(fontSize: 18.0),
            ),
          ),
          Container(
            // TEXT RESULT
            width: MediaQuery.of(context).size.width * 0.8,
            margin: EdgeInsets.symmetric(vertical: 20),
            decoration: BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.circular(6.0),
            ),
            padding: EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 12.0,
            ),
            child: Text(
              resultText,
              style: TextStyle(fontSize: 18.0, color: Colors.white),
            ),
          ),
          Container(
            // Mic Button
            width: 250,
            height: 250,
            margin: EdgeInsets.symmetric(vertical: 100),
            child: FloatingActionButton(
              child: Icon(
                Icons.mic,
                size: 100,
              ),
              onPressed: () async {
                if (!_isListening) {
                 await GetText();
                }
              },
              backgroundColor: Colors.pink,
            ),
          ),
        ],
      ),
    );
  }
}
