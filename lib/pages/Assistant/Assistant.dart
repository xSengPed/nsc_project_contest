import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:nsc_contest_robot/pages/Assistant/AssistantModule.dart';

class Assistant extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Kanit'),
      home: Scaffold(
        appBar: AppBar(
          title: Text('โหมดผู้ช่วย'),
        ),
        body: AssistComponent(),
      ),
    );
  }
}

class AssistComponent extends StatelessWidget {
  final FlutterTts flutterTts = new FlutterTts();

  void Speak(text) async {
    await flutterTts.setLanguage("th-TH");
    await flutterTts.speak(text);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Tooltips(),
          AssistantMic(),
        ],
      ),
    );
  }
}

class Tooltips extends StatelessWidget {
  final double toolTipFont = 16;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      alignment: Alignment.center,
      child: Text(
        "ท่านสามารถใช้งานคำสั่งเสียงโดยการ กดปุ่ม รูปไมค์ตรงกลาง",
        style: TextStyle(fontSize: toolTipFont),
      ),
    );
  }
}

class AssistantMic extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      
      child: VoiceHome()
    
    );
  }
}
