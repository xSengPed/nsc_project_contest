import 'package:flutter/material.dart';

class ControlMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Kanit'),
      home: Scaffold(
        appBar: AppBar(
          title: Text("โหมดควบคุม"),
        ),
        body: Container(
          
          margin: EdgeInsets.symmetric(vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[BluetoothConnect(), SizedBox(height: 100,) ,ControlDirection()],
          ),
        ),
      ),
    );
  }
}

class BluetoothConnect extends StatelessWidget {
  final double boxHeight = 20;
  final double font = 24;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        
        children: <Widget>[
          Text(
            'Bluetooth Connection',
            style: TextStyle(fontSize: font),
          ),
          SizedBox(
            height: boxHeight,
          ),
          RaisedButton(
            child: Text(
              'Connect',
              style: TextStyle(fontSize: 16),
            ),
            padding: EdgeInsets.symmetric(horizontal: 100, vertical: 10),
            onPressed: () {},
            textColor: Colors.white,
            color: Colors.blueAccent,
          ),
        ],
      ),
    );
  }
}

class ControlDirection extends StatelessWidget {
  final double buttonDim = 25;
  @override
  Widget build(BuildContext context) {
    return Container(
      
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizeBoxSpace(),
              RaisedButton(
                padding: EdgeInsets.symmetric(vertical: buttonDim,horizontal: buttonDim),
                child: Text('ไปหน้า'),
                onPressed: () {},
              ),
              SizeBoxSpace(),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                padding: EdgeInsets.symmetric(vertical: buttonDim,horizontal: buttonDim),
                child: Text('หมุนซ้าย'),
                onPressed: () {},
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(vertical: buttonDim,horizontal: buttonDim),
                child: Text('ถอย'),
                onPressed: () {},
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(vertical: buttonDim,horizontal: buttonDim),
                child: Text('หมุนขวา'),
                onPressed: () {},
              ),
            ],
          )
        ],
      ),
    );
  }
}
class SizeBoxSpace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      // decoration: BoxDecoration(
      //   color: Colors.blueAccent,
      // ),
      child: SizedBox(
        width: 100,
        height: 100,
      ),
    );
  }

}
